base     hyperwiki
author   pekman
email    pekman@hyperbola.info
date     2017/09/22
name     HYPERWIKI Dokuwiki Template
desc     Another simple, clean and responsive theme for dokuwiki, based on STM theme by Fraina (https://github.com/Fraina/STM-Dokuwiki-Template).
url      https://notabug.org/hyperbola/hyperwiki
