<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

<!-- ********** HEADER ********** -->

<header class="header">
    	<div id="archnavbar" class="anb-home">
        <div id="archnavbarlogo"><h1><a href="https://www.hyperbola.info/" title="Hyperbola GNU/Linux-Libre">Hyperbola GNU/Linux-Libre</a></h1></div>
        <div id="archnavbarmenu">
            <ul id="archnavbarlist">
                <li id="anb-home"><a href="https://www.hyperbola.info/" title="Hyperbola GNU/Linux-libre">Home</a></li>
                <li id="anb-packages"><a href="https://www.hyperbola.info/packages/" title="Hyperbola GNU/Linux-libre Package Database">Packages</a></li>
                <li id="anb-forums"><a href="https://forums.hyperbola.info/" title="HyperForum | Community Support">Forums</a></li>
                <li id="anb-wiki"><a href="https://wiki.hyperbola.info/" title="HyperWiki | Community Documentation">Wiki</a></li>
                <li id="anb-issues"><a href="https://issues.hyperbola.info/" title="Report and Track on Issues">Issues</a></li>
		<li id="anb-security"><a href="https://security.hyperbola.info/" title="Hyperbola Security  Tracker">Security</a></li>
		<li id="anb-git"><a href="https://git.hyperbola.info:50100/" title="Git Projects Code">Git</a></li>
		<li id="anb-download"><a href="https://www.hyperbola.info/download/" title="Get Hyperbola">Download</a></li>
            </ul>
        </div>
        </div>
        <div class="header-content">
        <hgroup class="header-heading fl">
            <h1 class="header-title prs">
                <?php
                    // display logo and wiki title in a link to the home page
                    tpl_link(
                        wl(),
                        '<span>'.$conf['title'].'</span>',
                        'accesskey="h" title="[H]"'
                    );
                ?>
            </h1>
            <?php if ($conf['tagline']): ?>
                <h5 class="header-subTitle tsi"><?php echo $conf['tagline']; ?></h5>
            <?php endif ?>
        </hgroup>
           <div class="header-nav fr pc-only">
            <div class="header-navButton--menuTools has-subList">
                <a class="icon-cube"></a>
                <ul>
                    <?php
                            tpl_action('edit', 1, 'li');
                            tpl_action('revert', 1, 'li');
                            tpl_action('backlink', 1, 'li');
                            tpl_action('recent', 1, 'li');
                            tpl_action('media', 1, 'li');
                            tpl_action('index', 1, 'li');
                    ?>
                </ul>
            </div>
            <div class="header-navButton--userTools has-subList">
                <a class="icon-user"></a>
                <ul>
                    <?php
                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">';
                            tpl_userinfo(); /* 'Logged in as ...' */
                            echo '</li>';
                        }
                        tpl_action('admin', 1, 'li');
                        tpl_action('profile', 1, 'li');
                        tpl_action('register', 1, 'li');
                        tpl_action('login', 1, 'li');
                    ?>
                </ul>
            </div>
            <div class="header-navButton--searchBar">
                <?php tpl_searchform(); ?>
                <a class="icon-search"></a>
            </div>
        </div>
        <div class="header-nav fr not-pc">
          <div class="header-navButton">
            <a class="icon-cube"></a>
          </div>
      </div>
</header>

<div class="nav-for-device">
  <div class="header-navButton--searchBar">
      <?php tpl_searchform(); ?>
      <a class="icon-search"></a>
  </div>
  <ul>
    <?php
            tpl_action('edit', 1, 'li');
            tpl_action('backlink', 1, 'li');
            tpl_action('recent', 1, 'li');
            tpl_action('index', 1, 'li');
    ?>
    <?php
        if (!empty($_SERVER['REMOTE_USER'])) {
            echo '<li class="user">';
            tpl_userinfo(); /* 'Logged in as ...' */
            echo '</li>';
        }
        tpl_action('admin', 1, 'li');
        tpl_action('profile', 1, 'li');
        tpl_action('register', 1, 'li');
        tpl_action('login', 1, 'li');
    ?>
  </ul>
</div>
